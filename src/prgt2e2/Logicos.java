/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Logicos {

   public static void main (String args[]) {
      int a=1,b=2,c=1;
      boolean e;
      e= a>b && c==a;
      System.out.println("1. "+e);
      e= a>b || c==a;
      System.out.println("2. "+e);
      e= (a>b || c==a) && (a!=c);
      System.out.println("3. "+e);
   }
}
/* EJECUCION:
1. false
2. true
3. false
*/

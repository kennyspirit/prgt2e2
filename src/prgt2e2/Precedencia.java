/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Precedencia {

   public static void main (String args[]) {
   int a;

      a=2+5*3; // * mas precedencia que +.
      System.out.println("1. "+a);
      a=2+(5*3);
      System.out.println("2. "+a);
      a=2+5*1+4/2; 
      System.out.println("3. "+a);
   }
}
/* EJECUCION:
1. 17
2. 17
3. 9
*/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
class Ascii
{
  public static void main(String [] args)
  {
    int i,j;
    char c;
    
    System.out.println("Ascii entre A y z");
    for(c = 'A',j=1; c <= 'z'; c++,j++) {
      i=(int)c;
      System.out.print(i+": "+c+"\t");
      if ( j%5 == 0) System.out.println("");
    }
    
    System.out.println("\nAscii entre a y z");
    for(c = 'a',j=1; c <= 'z'; c++,j++) {
      i=(int)c;
      System.out.print(j+"  "+i+":  "+c+"\t");
      if ( j%5 == 0) System.out.println("");
    }
    --j;
    System.out.print("\nLetras entre a y z son "+j);
    
  }
}
/* EJECUCION:
Ascii entre A y z
65: A	66: B	67: C	68: D	69: E	70: F	71: G	72: H	73: I	
74: J	75: K	76: L	77: M	78: N	79: O	80: P	81: Q	82: R	
83: S	84: T	85: U	86: V	87: W	88: X	89: Y	90: Z	91: [	
92: \	93: ]	94: ^	95: _	96: `	97: a	98: b	99: c	100: d	
101: e	102: f	103: g	104: h	105: i	106: j	107: k	108: l	109: m	
110: n	111: o	112: p	113: q	114: r	115: s	116: t	117: u	118: v	
119: w	120: x	121: y	122: z	

Ascii entre a y z
1  97:  a	2  98:  b	3  99:  c	4  100:  d	5  101:  e	
6  102:  f	7  103:  g	8  104:  h	9  105:  i	10  106:  j	
11  107:  k	12  108:  l	13  109:  m	14  110:  n	15  111:  o	
16  112:  p	17  113:  q	18  114:  r	19  115:  s	20  116:  t	
21  117:  u	22  118:  v	23  119:  w	24  120:  x	25  121:  y	
26  122:  z	
Letras entre a y z son 26
*/
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Unarios {
  public static void main (String args[]) {
    int a=1;
    a++; // a vale 2
    System.out.println("1. "+a);
    a= 1 + -a; // a vale 1-2=-1
    System.out.println("2. "+a);
    a= 0 + (++a); // a vale 0 -1 +1 = 0
    System.out.println("3. "+a);
    a= 0 + (a++); // a vale 0 + 0 = 0
    System.out.println("4. "+a);
  }
}
/* EJECUCION:
1. 2
2. -1
3. 0
4. 0
*/

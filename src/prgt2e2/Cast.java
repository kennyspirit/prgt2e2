/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class Cast {

  public static void main (String args[]) {
    // Sin perida float=int
    int i1=2;
    float f1;
    f1=i1;
    System.out.println("1. "+f1);
    // Perdida. int = double
    int i2=10;
    double d1=3.1416;
    i2= (int)d1;
    System.out.println("2. "+i2);
    // Sin perdida por mayor precision. short=byte
    short s1;
    byte b1=3;
    s1=b1;
    System.out.println("3. "+s1);
    // Error. Por perdida de precision
    short s2=200;
    byte b2; // bytes numero maximo 127
    // b2=s2;  // Error
  }
}
/* EJECUCION:
1. 2.0
2. 3
3. 3
* */

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package prgt2e2;

/**
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 */
public class TipoBoolean
{
  public static void main(String[] arg)
  {
    boolean valor1;
    valor1 = true;
    if (valor1) {
      System.out .println("valor1 = verdadero");
    } else {
      System.out .println("valor1 = falso");
    }
  }
}
/* EJECUCION
valor1 = verdadero
*/
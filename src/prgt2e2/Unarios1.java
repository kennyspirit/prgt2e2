package prgt2e2;

/**
 * @author Paco Aldarias
 */

public class Unarios1 {

  public static void main(String arg[]) {
    int i = 1;
    System.out.println(i++);
    System.out.println(i++);
  }
}
/* EJECUCION
 * 1
 * 2
 */